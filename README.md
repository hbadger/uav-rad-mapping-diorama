# UAV rad mapping diorama

An interactive diorama based on a recycled 3D printer to demonstrate the concepts of radiation mapping with UAVs.

Uses Repetier 3D printer firmware pinouts.
