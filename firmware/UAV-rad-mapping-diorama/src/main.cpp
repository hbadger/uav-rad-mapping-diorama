#include <Arduino.h>
#include <userpins.h>

//#define ORIG_X_STEP_PIN     15
//#define ORIG_X_DIR_PIN      14
//#define ORIG_X_MIN_PIN      11
//#define ORIG_X_MAX_PIN      -1
//#define ORIG_X_ENABLE_PIN   29

void move(uint8_t stepper, uint16_t steps);

bool home = true;

void setup() {
  // set up pins for X motor
  pinMode(ORIG_X_STEP_PIN, OUTPUT);
  pinMode(ORIG_X_DIR_PIN, OUTPUT);
  pinMode(ORIG_X_MIN_PIN, INPUT); // assumed to be the end switch
  pinMode(ORIG_X_MAX_PIN, INPUT); // not assigned on the Due
  pinMode(ORIG_X_ENABLE_PIN, OUTPUT);

  // set up pins for Y motor
  pinMode(ORIG_Y_STEP_PIN, OUTPUT);
  pinMode(ORIG_Y_DIR_PIN, OUTPUT);
  pinMode(ORIG_Y_MIN_PIN, INPUT); // assumed to be the end switch
  pinMode(ORIG_Y_MAX_PIN, INPUT); // not assigned on the Due
  pinMode(ORIG_Y_ENABLE_PIN, OUTPUT);

  // set up pins for the extruder motor
  //pinMode(ORIG_E1_STEP_PIN, OUTPUT);
  //pinMode(ORIG_E1_DIR_PIN, OUTPUT);
  //pinMode(ORIG_E1_STEP_PIN, OUTPUT);
  pinMode(ORIG_E0_DIR_PIN, OUTPUT);
  pinMode(ORIG_E0_ENABLE_PIN, OUTPUT);
  pinMode(ORIG_E0_STEP_PIN, OUTPUT);

  // set up pins for Z motor
  pinMode(ORIG_Z_STEP_PIN, OUTPUT);
  pinMode(ORIG_Z_ENABLE_PIN, OUTPUT);
  pinMode(ORIG_Z_DIR_PIN, OUTPUT);

  pinMode(LIGHT_PIN, OUTPUT);
  digitalWrite(LIGHT_PIN, HIGH);

  pinMode(BUZZER_PIN, OUTPUT);
  for (uint16_t x = 0; x < 1000; x++)
  {
  digitalWrite(BUZZER_PIN, HIGH);
  delay(1);
  digitalWrite(BUZZER_PIN, LOW);
  delay(1);
  }


  SerialUSB.begin(9600);
  while (!SerialUSB);
  SerialUSB.println("testing ports");
 
  // toggle pin
  uint8_t forbidden_pins [2] = {99, 102};
  for (uint8_t pin = 95; pin < 145; pin++)
  {
    SerialUSB.print("testing pin "); SerialUSB.println(pin);
    bool forbidden = false;
    for (uint8_t search = 0; search < sizeof(forbidden_pins); search++)
      if (forbidden_pins[search] == pin)
        forbidden = true;
    
    if (forbidden = true)
    {
      SerialUSB.print("skipping pin "); SerialUSB.println(pin);
      forbidden = false;
    } else {
    pinMode(pin, OUTPUT);
    for (uint16_t pulse = 1; pulse < 500; pulse++)
    {
      digitalWrite(pin, HIGH);
      delay(2);
      digitalWrite(pin, LOW);
      delay(2);
    }
  }
  }




  // enable driver
  // drivers are enabled ACTIVE LOW
  digitalWrite(ORIG_X_ENABLE_PIN, LOW);
  digitalWrite(ORIG_Y_ENABLE_PIN, LOW);
  digitalWrite(ORIG_E0_ENABLE_PIN, LOW);
  digitalWrite(ORIG_Z_ENABLE_PIN, LOW);
  //digitalWrite(ORIG_E1_ENABLE_PIN, LOW);

  // homing sequence
  // homing X-axis



  //while (1)
  //{
  //  Serial.println(digitalRead(ORIG_Y_MAX_PIN));
  //  delay(100);
  //}
  


  // homing Y-axis
  // while (home)
  // {
  // digitalWrite(ORIG_Y_DIR_PIN, LOW);
  // while (!digitalRead(ORIG_Y_MIN_PIN))
  // {
  //   digitalWrite(ORIG_Y_STEP_PIN, HIGH);
  //   delayMicroseconds(40);
  //   digitalWrite(ORIG_Y_STEP_PIN, LOW);
  //   delayMicroseconds(40);
  // }

  // digitalWrite(ORIG_X_DIR_PIN, HIGH);
  // while (!digitalRead(ORIG_X_MIN_PIN))
  // {
  //   digitalWrite(ORIG_X_STEP_PIN, HIGH);
  //   delayMicroseconds(30);
  //   digitalWrite(ORIG_X_STEP_PIN, LOW);
  //   delayMicroseconds(30);
  // }
  // }
  
  


}

void loop() {

while (home)
  {
  digitalWrite(ORIG_Y_DIR_PIN, LOW);
  while (!digitalRead(ORIG_Y_MIN_PIN))
  {
    digitalWrite(ORIG_Y_STEP_PIN, HIGH);
    delayMicroseconds(40);
    digitalWrite(ORIG_Y_STEP_PIN, LOW);
    delayMicroseconds(40);
  }

  digitalWrite(ORIG_X_DIR_PIN, HIGH);
  while (!digitalRead(ORIG_X_MIN_PIN))
  {
    digitalWrite(ORIG_X_STEP_PIN, HIGH);
    delayMicroseconds(30);
    digitalWrite(ORIG_X_STEP_PIN, LOW);
    delayMicroseconds(30);
  }
  home = false;
  }

  // move motors in positive direction
  digitalWrite(ORIG_X_DIR_PIN, LOW);
  digitalWrite(ORIG_Y_DIR_PIN, HIGH);
  digitalWrite(ORIG_E0_DIR_PIN, LOW);
  digitalWrite(ORIG_Z_DIR_PIN, LOW);

  move(ORIG_X_STEP_PIN, 20000);
  move(ORIG_Y_STEP_PIN, 15000);
  move(ORIG_E0_STEP_PIN, 10000);
  move(ORIG_Z_STEP_PIN, 10000);

  // move motors in negative direction
  digitalWrite(ORIG_X_DIR_PIN, HIGH);
  digitalWrite(ORIG_Y_DIR_PIN, LOW);
  digitalWrite(ORIG_E0_DIR_PIN, HIGH);

  move(ORIG_X_STEP_PIN, 20000);
  move(ORIG_Y_STEP_PIN, 15000);
  move(ORIG_E0_STEP_PIN, 10000);


  

}

void move(uint8_t stepper, uint16_t steps)
{
  for (uint16_t step = 0; step < steps; step++)
  {
    digitalWrite(stepper, HIGH);
    delayMicroseconds(40);
    digitalWrite(stepper, LOW);
    delayMicroseconds(40);
  }

}